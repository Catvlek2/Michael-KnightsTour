#include <cstdio>
#include <ctime>
#include <iostream>
using namespace std;

int N = 5; //This is the size of the board
int ** board; // Pointer of the matrix

struct moves {
	int x, y; //moves the knight can make
};

moves knight_pos;
moves current_pos = { 0,0 }; //Starting position

//checks if the move is inside the 5x5 board and if the tile is unvisited
bool hasTiles(moves next_move) {
	if ((next_move.x >= 0 && next_move.x < N) && (next_move.y >= 0 && next_move.y < N) && (board[next_move.x][next_move.y] == 0))
		return true;
	else 
		return false;
}

// recursive function to find a knight tour
bool generateTour(int move_count, moves current_pos) {
	moves next_move;
	if (move_count == N*N - 1) { //if knight visited all tiles except starting tile(-1), it is completed
		return true;
	}

	// all possible moves a knight can make
	moves knight_move[8] = { { 2,1 },{ 1,2 },{ -1,2 },{ -2,1 },{ -2,-1 },{ -1,-2 },{ 1,-2 },{ 2,-1 } };

	for (int i = 0; i < 8; i++) //loops through all possible moves 
	{
		next_move.x = current_pos.x + knight_move[i].x; //Updates the knights x position
		next_move.y = current_pos.y + knight_move[i].y; //Updates the knights y position

		if (hasTiles(next_move)) {
			board[next_move.x][next_move.y] = move_count + 1; //new value saved in the tile 
			if (generateTour(move_count + 1, next_move) == true) { //this is the recursive part
				return true;

			}
			else {
				board[next_move.x][next_move.y] = 0; // back tracks and reverts value back to 0 
			}
		}

	}
	return false;
}

// displays solution in a  beautiful box
void displaySolution() {
	cout << endl << "----------------------------------------------------------" << endl;
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			cout << "| " << board[i][j] << "\t |";
		}
		cout << endl << "------------------------------------------------------------" << endl;
	}
}

bool GetNumberInput(int min, int max, int &input)
{
	if (cin >> input)
	{
		if (input >= min && input <= max)
		{
			cout << "Your input meets the criteria" << endl;
			return true;
		}
		else cout << "Your input does not meet the criteria" << endl;
		return false;
	}
	else
	{
		cin.clear(); 
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // ignore bad input
		cout << "Your input was wrong, please try again!" << endl;
		return false;
	}
}

void startTour() {
	// Matrix is filled with unvisited tiles.
	board = new int*[N];
	for (int i = 0; i < N; i++) {
		board[i] = new int[N];
		for (int j = 0; j < N; j++) {
			board[i][j] = 0;
		}
	}
	cout << "This program will try to solve the Knights Tour on a boardsize of your choice starting from position: (0,0); \n\n";

	if (generateTour(0, current_pos) == true) {
		displaySolution();
		cout << "\n\nThis is the Knight's tour completed with your chosen boardsize \n";
	}
}


// main
int main() {

	do {
		cout << "please enter a size for the playing board (5 - 8): ";
	} while (!GetNumberInput(5, 8, N));

	startTour();
	system("pause");
	return 0;
}